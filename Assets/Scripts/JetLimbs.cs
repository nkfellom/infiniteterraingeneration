﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JetLimbs : MonoBehaviour {

    public float jetPower = 20.0f;

    public Rigidbody leftHand;
    public Rigidbody rightHand;
    public Rigidbody leftFoot;
    public Rigidbody rightFoot;

    private List<Rigidbody> rigs;

	// Use this for initialization
	void Start () {
        rigs = new List<Rigidbody>
        {
            leftFoot,
            rightFoot,
            leftHand,
            rightHand
        };
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Space))
        {
            foreach(var r in rigs)
            {
                r.AddForce(r.transform.localPosition * jetPower, ForceMode.Impulse);
            }
        }
	}
}
