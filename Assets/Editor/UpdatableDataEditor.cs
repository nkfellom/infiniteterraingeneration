﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR

[UnityEditor.CustomEditor(typeof(UpdatableData), true)]
public class UpdatableDataEditor : UnityEditor.Editor {


    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        UpdatableData data = (UpdatableData)target;

        if(GUILayout.Button("Update"))
        {
            data.NotifyOfUpdatedValues();
            UnityEditor.EditorUtility.SetDirty(target);
        }
    }

}

#endif
